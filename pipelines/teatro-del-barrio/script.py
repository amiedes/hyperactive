import pdb

from datetime import datetime
from elasticsearch import Elasticsearch

class EventDoc:

    SOURCE_ID = 'teatro-del-barrio'

    def __init__(self, attrs):
        self.title = attrs['title']
        self.description = attrs['description']
        self.starts_at = attrs['starts_at']
        self.ends_at = attrs['ends_at']
        self.source_url = attrs['source_url']
        self.source_id = self.SOURCE_ID
    
client = Elasticsearch()

event = EventDoc({
    'title': 'Concierto en el Teatro del Barrio',
    'description': 'Concierto muy guay en el Teatro del Barrio',
    'starts_at': datetime(2019, 3, 25, 21, 30),
    'ends_at': datetime(2019, 3, 26, 00, 00),
    'source_url': 'http://example.com/evento-1'
})

res = client.index(
    index="events",
    doc_type='event',
    id=1,
    body=event.__dict__
)

print(res['result'])
